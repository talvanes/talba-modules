<?php namespace Talba\Modules\Contracts;

/**
 * This contract describes a module manager, or coordinator.
 *  It manages adding, removing, loading and unloading of modules.
 *
 * @package Talba\Modules\Contracts
 */
interface ModuleManagerInterface
{
    /**
     * Sets initial modules to load.
     *
     * @param array $modules A module list to load
     * @return $this The manager instance itself
     */
    public function setModules(array $modules);

    /**
     * Adds a module to the coordinator.
     *
     * @param string $name Name of module to add
     * @return $this The manager instance itself
     */
    public function addModule(string $name);

    /**
     * Removes a module from the coordinator.
     *
     * @param string $name Name of module to remove
     * @return $this The manager instance itself
     */
    public function removeModule(string $name);

    /**
     * Loads an existing module.
     *
     * @param string $name Name of module to load
     * @return $this The manager instance itself
     */
    public function loadModule(string $name);

    /**
     * Unloads an existing module.
     *
     * @param string $name Name of module to unload
     * @return $this The manager instance itself
     */
    public function unloadModule(string $name);
}