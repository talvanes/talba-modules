<?php namespace Talba\Modules\Contracts;

/**
 * This contract extends an AppAdapterInterface instance to give it a web application behaviour.
 *  It means with this interface, it acts like a true web application.
 *
 * @package Talba\Modules\Contracts
 */
interface WebAwareInterface
{
    /**
     * Sets a controller action, normally one closure, not an entire controller class.
     *  That is, it is action-oriented, as with the action-domain-responder (ADR) pattern.
     *  You can also use invokable classes in place of closures.
     *
     * @param string $name The action name for reference in the application
     * @param callable $callable The action code defined as a valid PHP callable
     * @return $this An application adapter instance
     */
    public function addAction(string $name, callable $callable);

    /**
     * @param string $name The middleware name for reference in the application
     * @param callable $callable Middleware code defined as a valid PHP callable
     * @return $this An application adapter instance
     */
    public function addMiddleware(string $name, callable $callable);

    /**
     * @param string $method A valid HTTP method name (e.g. GET, POST, PUT, PATCH, DELETE, etc.)
     * @param string $pattern Glob pattern to describe the route
     * @param callable $action The action code defined as a valid PHP callable
     * @return $this An application adapter instance
     */
    public function addRoute(string $method, string $pattern, callable $action);
}