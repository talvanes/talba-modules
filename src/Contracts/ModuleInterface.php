<?php namespace Talba\Modules\Contracts;

/**
 * This interface describes a module, or a piece of code that loads settings and dependencies needed for it to work.
 *
 * @package Talba\Modules\Contracts
 */
interface ModuleInterface
{
    /**
     * Loads a module.
     *
     * @return $this
     */
    public function load();

    /**
     * Unloads a module.
     *
     * @return $this
     */
    public function unload();


    /**
     * Sets callable to load before the module starts, that is, the module dependencies.
     *
     * @param callable $code Any valid PHP callable
     * @return $this The module instance itself
     */
    public function setProvider(callable $code);

    /**
     * Returns the service provider code that corresponds to this module.
     *
     * @return callable Service provider code
     */
    public function getProvider();


    /**
     * Sets configuration parameter with the value given.
     *
     * @param string $key Configuration key
     * @param mixed $value Value of ANY type (e.g. integer, float, string, array, object, callable, user-defined class, etc.)
     * @return $this The module instance itself
     */
    public function setConfig(string $key, $value);

    /**
     * Gets configuration parameter by name.
     *
     * @param string $key Configuration key
     * @return mixed Any value
     */
    public function getConfig(string $key);
}