<?php namespace Talba\Modules\Contracts;

/**
 * This contract describes an adapter for a generic application (web, console, desktop, mobile),
 *  whether it is merely a library or part of a third-party framework.
 *
 * @package Talba\Modules\Contracts
 */
interface AppAdapterInterface
{
    /**
     * Sets and initializes application settings.
     * It accepts an array of settings in the form:
     *  [
     *      "key" => $value,
     *      (...)
     *  ]
     * where:
     *  "key" is string, and
     *  $value is of ANY type: string, integer, float, callable, array, object, user-defined class, etc.
     *
     * @param array $settings An array of settings referenced by key and value.
     * @return $this An application adapter instance
     */
    public function initSettings(array $settings);

    /**
     * Sets a dependency callable (service provider), and gives it a name for later use in the application.
     *
     * @param string $name The dependency name for reference in the application
     * @param callable $callable A valid PHP callable for the dependency code
     * @return $this An application adapter instance
     */
    public function addDependency(string $name, callable $callable);

    /**
     * Runs the application.
     *
     * @return void
     */
    public function run();
}