<?php namespace Talba\Modules\Adapters;

use Talba\Modules\Contracts\{
    AppAdapterInterface,
    WebAwareInterface
};

/**
 * Slim 3 Application adapter for using it with the modules system.
 *
 * @package Talba\Modules\Adapters
 */
class SlimAppAdapter implements AppAdapterInterface, WebAwareInterface
{
    /** @var  \Slim\App */
    protected $app;
    /** @var  \Psr\Container\ContainerInterface */
    protected $container;

    /** @var  \Psr\Http\Message\ServerRequestInterface */
    protected $request;
    /** @var  \Psr\Http\Message\ResponseInterface */
    protected $response;

    /** @var  array */
    protected $settings;

    /** @var  callable[] */
    protected $actions;
    /** @var  callable[] */
    protected $middlewares;
    /** @var  callable[] */
    protected $routes;
    /** @var  callable[] */
    protected $providers;

    /**
     * {@inheritdoc}
     */
    public function initSettings(array $settings)
    {
        // TODO: Implement initSettings() method.
    }

    /**
     * {@inheritdoc}
     */
    public function addDependency(string $name, callable $callable)
    {
        // TODO: Implement addDependency() method.
    }

    /**
     * {@inheritdoc}
     */
    public function addAction(string $name, callable $callable)
    {
        // TODO: Implement addAction() method.
    }

    /**
     * {@inheritdoc}
     */
    public function addMiddleware(string $name, callable $callable)
    {
        // TODO: Implement addMiddleware() method.
    }

    /**
     * {@inheritdoc}
     */
    public function addRoute(string $method, string $pattern, callable $action)
    {
        // TODO: Implement addRoute() method.
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        // TODO: Implement run() method.
    }
}