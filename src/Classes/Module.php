<?php namespace Talba\Modules\Classes;

use Talba\Modules\Contracts\ModuleInterface;

abstract class Module implements ModuleInterface
{
    /**
     * The module name.
     *
     * @var string
     */
    protected $name;

    /**
     * Module settings, in the form:
     *  [
     *      "key" => $value,
     *      (...)
     *  ],
     *  where:
     *      "key" is string, and
     *      $value is of ANY type (e.g. integer, float, string, array, object, callable, user-defined class, etc.)
     *
     * @var array
     */
    protected $settings;

    /**
     * Dependency provider code, which is needed
     *
     * @var callable|null
     */
    protected $provider;

    /**
     * Module constructor.
     * It creates a new module given a name for it.
     *
     * @param string $name
     */
    public function __construct(string $name) {
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function load() {
        // Load settings
        $this->loadSettings();
        // Load service provider
        $this->loadProvider();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function unload() {
        // TODO: Implement unload() method.
    }


    /**
     * {@inheritdoc}
     */
    public function setProvider(callable $code)
    {
        // TODO: Implement setProvider() method.
    }

    /**
     * {@inheritdoc}
     */
    public function getProvider()
    {
        // TODO: Implement getProvider() method.
    }

    /**
     * {@inheritdoc}
     */
    public function setConfig(string $key, $value)
    {
        // TODO: Implement setConfig() method.
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(string $key)
    {
        // TODO: Implement getConfig() method.
    }


    /**
     * Loads dependencies of the module.
     *
     * @return $this
     */
    abstract protected function loadProvider();

    /**
     * Loads the module settings.
     *
     * @return $this
     */
    abstract protected function loadSettings();


    // TODO: declare unloads

}