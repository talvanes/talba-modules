<?php namespace Talba\Modules\Classes;

use Talba\Modules\Contracts\ModuleManagerInterface;

abstract class ModuleManager implements ModuleManagerInterface
{
    /**
     * A list of modules to load.
     * It must be a name list.
     *
     * @var array
     */
    protected $modules;

    /**
     * ModuleManager constructor.
     *
     * @param array $modules [optional] Modules to set before loading
     */
    public function __construct(array $modules = []) {
        $this->setModules($modules);
    }
}